package mk.test.programmingtablayout;

public class Language {

    private String name, category, year;

    public Language (){}

    public Language (String name, String category, String year){
        this.name = name;
        this.category = category;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
