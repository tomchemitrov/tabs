package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mk.test.programmingtablayout.Language;
import mk.test.programmingtablayout.R;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.LanguageViewHolder>{

    List<Language> languages;
    LayoutInflater inflater;

    public LanguageAdapter(Context context, List<Language> languages){
        this.languages = languages;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public LanguageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =inflater.inflate(R.layout.item_prog_lang,parent,false);
        return new LanguageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LanguageViewHolder holder, int position) {
        Language data = languages.get(position);

        holder.name.setText(data.getName());
        holder.category.setText(data.getCategory());
        holder.year.setText(data.getYear());
    }

    @Override
    public int getItemCount() {
        return languages.size();
    }

    public class LanguageViewHolder extends RecyclerView.ViewHolder {

        TextView name, category, year;

        public LanguageViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.languageName);
            category = itemView.findViewById(R.id.languageCategory);
            year = itemView.findViewById(R.id.languageYear);
        }
    }
}
