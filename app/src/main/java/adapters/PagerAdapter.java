package adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import fragments.FragmentExplore;

public class PagerAdapter extends FragmentStatePagerAdapter {

    int numOfTabs;

    public PagerAdapter(@NonNull FragmentManager fm, int num) {
        super(fm);
        this.numOfTabs = num;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return new FragmentExplore(position);
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
