package fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import adapters.LanguageAdapter;
import mk.test.programmingtablayout.Language;
import mk.test.programmingtablayout.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentExplore extends Fragment {

    RecyclerView recyclerView;
    LanguageAdapter adapter;
    List<Language> languages = new ArrayList<>();
    int position;

    public FragmentExplore(int position) {
        this.position = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_explore, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        generateLanguages();

        adapter = new LanguageAdapter(getActivity(),languages);
        recyclerView.setAdapter(adapter);
    }

    void generateLanguages(){

        Language l1 = new Language("PHP", "WEB Programming","1945");
        Language l2 = new Language("JavaScript", "WEB Programming","1988");
        Language l3 = new Language("Java", "Programming","1915");
        Language l4 = new Language("C#", "WEB Programming","1945");
        Language l5 = new Language("Python", "Programming","1945");
        Language l6 = new Language("HTML", "WEB Programming","1945");
        Language l7 = new Language("PHP", "WEB Programming","1945");
        Language l8 = new Language("iOS", "WEB Programming","1945");
        Language l9 = new Language("PHP", "WEB Programming","1945");

        languages.add(l1);
        languages.add(l2);
        languages.add(l3);
        languages.add(l4);
        languages.add(l5);
        languages.add(l6);
        languages.add(l7);
        languages.add(l8);
        languages.add(l9);
    }
}
